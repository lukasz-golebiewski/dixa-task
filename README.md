## Readme

This is a solution for the Dixa Backend Engineer Test

It is implemented in Scala with help of:
* `mu-scala` - used for gRPC
* `http4s` - used for the http server
* `fs2` - used for streaming support in the http server

Those frameworks were chosen for their native functional programming support.

### Usage

1. Start the prime number server:

```
sbt
sbt:dixa-task> project primeNumberServer
sbt:prime-number-server> run
[info] running (fork) com.lgo.Server
```

2. Afterwards start the proxy service:

```
sbt
sbt:dixa-task> project proxyService
sbt:proxy-service> run
[info] running (fork) com.lgo.HttpServer
[info] [ioapp-compute-0] INFO org.http4s.blaze.channel.nio1.NIO1SocketServerGroup - Service bound to address /127.0.0.1:8080
[info] [ioapp-compute-0] INFO org.http4s.server.blaze.BlazeServerBuilder -
[info]   _   _   _        _ _
[info]  | |_| |_| |_ _ __| | | ___
[info]  | ' \  _|  _| '_ \_  _(_-<
[info]  |_||_\__|\__| .__/ |_|/__/
[info]              |_|
[info] [ioapp-compute-0] INFO org.http4s.server.blaze.BlazeServerBuilder - http4s v0.21.21 on blaze v0.14.15 started at http://127.0.0.1:8080/

```

3. Now it is possible to start issuing requests towards the server with e.g. httpie

```
nix-shell -p httpie

[nix-shell:~]$ http --stream localhost:8080/prime/17
HTTP/1.1 200 OK
Content-Type: text/plain; charset=UTF-8
Date: Fri, 07 May 2021 11:13:16 GMT
Transfer-Encoding: chunked

2
3
5
7
11
13
17

```
