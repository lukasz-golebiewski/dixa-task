package com.lgo

import cats.Applicative
import cats.syntax.applicative._
import com.lgo.primes._
import fs2.Stream

/** Naive implementation of the Primes service.
 */
class NaivePrimes[F[_]: Applicative] extends Primes[F] {

  /** Finds all prime numbers less than or equal than the parameter.
      Returns the result as a fs2 stream
    */
  def Find(req: Number): F[fs2.Stream[F, Number]] =
    Applicative[F].pure(Stream.range(2,req.value + 1).filter(isPrime(_)).map(Number(_)))

  def IsPrime(req: Number): F[Response] = Applicative[F].pure(Response(isPrime(req.value)))

  /** Checks whether a number is prime
    */
  def isPrime(n: Int): Boolean = {
    if(n == 1)
      return false
    (2 to (scala.math.sqrt(n)).toInt)
      .foreach(i => if(n % i == 0) return false)
    true
  }
}
