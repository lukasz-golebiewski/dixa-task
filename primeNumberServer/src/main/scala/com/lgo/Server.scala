package com.lgo

import cats.effect._
import com.lgo.primes.Primes
import higherkindness.mu.rpc.server._

/** gRPC Server.
    Exposes the Primes service
  */
object Server extends IOApp {

  implicit val primes: Primes[IO] = new NaivePrimes[IO]

  def run(args: List[String]): IO[ExitCode] = for {
    serviceDef <- Primes.bindService[IO]
    server     <- GrpcServer.default[IO](12345, List(AddService(serviceDef)))
    _          <- GrpcServer.server[IO](server)
  } yield ExitCode.Success

}
