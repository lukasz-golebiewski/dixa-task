package com.lgo

import cats.effect._

import org.scalatest.flatspec.AnyFlatSpec

class NaivePrimesSpec extends AnyFlatSpec {

  val tested = new NaivePrimes[IO]

  it should "check primes" in {
    val numbers = List(1,2,3,4,5,6,7,8,9,10)
    val result = numbers.zip(numbers.map(tested.isPrime))
    assert(result === List(
      (1, false),
      (2, true),
      (3, true),
      (4, false),
      (5, true),
      (6, false),
      (7, true),
      (8, false),
      (9, false),
      (10, false),
      )
    )
  }

}
