package com.lgo

import cats.Applicative
import cats.effect._
import com.lgo.primes._
import higherkindness.mu.rpc.server._
import higherkindness.mu.rpc.testing.servers.withServerChannel
import io.grpc.ManagedChannel
import org.scalatest.flatspec.AnyFlatSpec
import scala.concurrent.ExecutionContext.Implicits
import fs2.Stream

class ServerSpec extends AnyFlatSpec {

  val EC: scala.concurrent.ExecutionContext = Implicits.global

  implicit val timer: Timer[IO]     = IO.timer(EC)
  implicit val cs: ContextShift[IO] = IO.contextShift(EC)

  implicit val primes: Primes[IO] = new NaivePrimes[IO]

  /*
   * A cats-effect Resource that builds a gRPC server and client
   * connected to each other via an in-memory channel.
   */
  def clientResource: Resource[IO, Primes[IO]] = for {
    sc        <- withServerChannel(Primes.bindService[IO])
    clientRes <- Primes.clientFromChannel[IO](IO.pure(sc.channel))
  } yield clientRes

  behavior of "RPC server"

  it should "call the gRPC service and return smaller primes" in {
    val response: Vector[Number] = clientResource
      .use(client => client.Find(Number(17)).flatMap(_.compile.toVector))
      .unsafeRunSync()

    assert(response === Vector(2,3,5,7,11,13,17).map(Number(_)))
  }

  it should "call the gRPC service and perform the calculation" in {
    val response:Response = clientResource
      .use(client => client.IsPrime(Number(4)))
      .unsafeRunSync()
    assert(response === Response(false))
  }

}
