package com.lgo

import cats.effect._
import org.http4s._
import org.http4s.dsl.io._
import org.http4s.implicits._
import org.http4s.server.blaze._
import scala.concurrent.ExecutionContext.global
import org.http4s.circe._
import io.circe.Json

import com.lgo.primes._
import higherkindness.mu.rpc._

import fs2._
import scala.concurrent.duration._
import cats.syntax.all._

/** HttpServer exposes the primes endpoint.
  */
object HttpServer extends IOApp {

  val channelFor: ChannelFor = ChannelForAddress("localhost", 12345)

  val serviceClient: Resource[IO, Primes[IO]] = Primes.client[IO](channelFor)

  def service[F[_]](serviceClient: Resource[F, Primes[F]])(
        implicit F: Effect[F]
  ) = HttpRoutes.of[F] {
    case GET -> Root / "prime" / IntVar(number) =>
      F.pure(
       org.http4s.Response(status = Status.Ok)
        .withEntity(
          Stream.range[F](2,number + 1)
           .evalFilter(i => serviceClient.use(c => c.IsPrime(Number(i)).map(_.value)))
           .map(_.toString ++ ("\n"))
           .handleErrorWith {
             // Writing a malformed chunk and an error trailer header
             case _ => Stream("\n", "Error: An internal server error occurred")
           }
        )
      )
    case GET -> Root / "prime" / _ =>
      F.pure(org.http4s.Response(status = Status.BadRequest).withEntity("Expected a number"))
  }.orNotFound

  def run(args: List[String]): IO[ExitCode] =
    BlazeServerBuilder[IO](global)
      .bindHttp(8080, "localhost")
      .withHttpApp(service(serviceClient))
      .serve
      .compile
      .drain
      .as(ExitCode.Success)
}
