package com.lgo

import cats.Applicative
import cats.effect._
import io.grpc.ManagedChannel
import org.scalatest.flatspec.AnyFlatSpec
import scala.concurrent.ExecutionContext.Implicits
import fs2.Stream

import org.http4s._
import com.lgo.primes.Primes
import com.lgo.HttpServer._

class HttpServerSpec extends AnyFlatSpec {

  // Mock impl for test purposes.
  // Lies that every number is prime
  val mockPrimes: Primes[IO] = new Primes[IO] {
    def Find(req: com.lgo.primes.Number): cats.effect.IO[fs2.Stream[cats.effect.IO,com.lgo.primes.Number]] = ???
    def IsPrime(req: com.lgo.primes.Number): cats.effect.IO[com.lgo.primes.Response] = IO.pure(com.lgo.primes.Response(true))
  }
  val mockPrimesResource: Resource[IO, Primes[IO]] = Resource.make(IO.pure(mockPrimes))(_ => IO.pure())

  it should "respond to a correct request" in {
    val response: IO[Response[IO]] = service[IO](mockPrimesResource).run(
      Request(method = Method.GET, uri = Uri.uri("/prime/4") )
    )
    assert(check(response, Status.Ok, Some("2\n3\n4\n")))
  }

  it should "reject an incorrect request" in {
    val response: IO[Response[IO]] = service[IO](mockPrimesResource).run(
      Request(method = Method.GET, uri = Uri.uri("/prime/asdf") )
    )
    assert(check(response, Status.BadRequest, Some("Expected a number")))
  }

  it should "reject an incorrect route" in {
    val response: IO[Response[IO]] = service[IO](mockPrimesResource).run(
      Request(method = Method.GET, uri = Uri.uri("/primes") )
    )
    assert(check(response, Status.NotFound, Some("Not found")))
  }

  def check[A](actual:        IO[Response[IO]],
              expectedStatus: Status,
              expectedBody:   Option[A])(
      implicit ev: EntityDecoder[IO, A]
  ): Boolean =  {
     val actualResp         = actual.unsafeRunSync
     val statusCheck        = actualResp.status == expectedStatus
     // println(actualResp.body.compile.toVector.unsafeRunSync.map(_.toChar).mkString)
     val bodyCheck          = expectedBody.fold[Boolean](
         actualResp.body.compile.toVector.unsafeRunSync.isEmpty)( // Verify Response's body is empty.
         expected => actualResp.as[A].unsafeRunSync == expected
     )
     statusCheck && bodyCheck
  }
}
